<?php

class CPage
{
    public $template = false;

    static function getCurDir($index=false)
    {
        if ( strval(intval($index)) != $index )
            return false;

        $arCurDir = explode('/',$_SERVER['PHP_SELF']);

        if ( $index===false )
            return str_replace(end($arCurDir),'',$_SERVER['PHP_SELF']);

        else return "/$arCurDir[$index]/";
    }

    static function useTemplate($templateName)
    {
        global $DATA;
        require_once($_SERVER['DOCUMENT_ROOT']."/_service/templates/page/{$templateName}.php");
    }
}