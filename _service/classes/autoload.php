<?php

spl_autoload_register('autoloader');

function autoloader ($className) {
    $fileName = $className . '.php';
    if(file_exists($_SERVER['DOCUMENT_ROOT'].'/_service/classes/'.$fileName)) {
        include ($_SERVER['DOCUMENT_ROOT'].'/_service/classes/'.$fileName);
    }else{
        echo $_SERVER['DOCUMENT_ROOT'].'/_service/classes/'.$fileName;
    }
}