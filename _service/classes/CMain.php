<?php

class CMain
{

    const SAULT = 's4$%sad56asd%^&';
    const AUTH_PATH = '/auth/';
    const EMAIL_TEMPLATES_PATH = '/_service/emailTemplates/';

    const theme = 'basic';

    static function prnt($varSomething)
    {
        echo '<pre>';
        print_r($varSomething);
        echo '</pre>';
    }

    static function hash($var)
    {
        return md5($var.md5($var.self::SAULT).self::SAULT);
    }

    public function getThemePath()
    {
        return "/_service/themes/".self::theme."/";
    }

    static function sendLetter($email,$template,$arParams=false)
    {
        $templateFile = $_SERVER['DOCUMENT_ROOT'] . self::EMAIL_TEMPLATES_PATH . $template . '.php';
        $message = file_get_contents($templateFile);
        foreach ($arParams as $key => $value) $message = str_replace("#$key#",$value,$message);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            if (is_array($email))
            {
                foreach ($email as $key => $value)
                {
                    if (filter_var($value, FILTER_VALIDATE_EMAIL))
                    {
                        $mail['to']      = $value;
                        $mail['subject'] = 'Подтверждение email';
                        $mail['message'] = $message;
                        $mail['headers'] = "Content-type: text/html; charset=iso-8859-1 \r\n";
                    }
                }
            }
            else
                return 'Неверный email';
        }
        else
        {
            $mail['to']      = $email;
            $mail['subject'] = 'Подтверждение email';
            $mail['message'] = $message;
            $mail['headers'] = "Content-type: text/html; charset=iso-8859-1 \r\n";
        }
    }

}