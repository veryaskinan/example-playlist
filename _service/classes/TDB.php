<?php

trait TDB
{
    static function getColumns($full=false)
    {
        global $DB;

        $query = 'SHOW COLUMNS FROM '. self::table;
        $sqlResult = $DB->query($query);

        if ( $full=='FULL' )
        {
            while ( $row = $sqlResult->fetch_object() )
                $arResult[$row->Field] = $row;
        }
        else
        {
            while ( $row = $sqlResult->fetch_object() )
                $arResult[] = $row->Field;
        }

        return $arResult;
    }

    protected static function explodeFieldType ($varFieldType)
    {
        $varFieldType = str_replace(')','',$varFieldType);
        $arFieldType = explode('(',$varFieldType);
        $arFieldTypeFinal['type'] = $arFieldType[0];
        $arFieldTypeFinal['length'] = $arFieldType[1];
        return $arFieldTypeFinal;
    }

    protected static function checkFieldValue ($varValue,$varFieldType)
    {
        global $DB;

        $arFieldType = self::explodeFieldType($varFieldType);
        if ( $arFieldType['type']=='int' || $arFieldType['type']=='tinyint' )
        {

            if ( !ctype_digit($varValue))
                $result = false;
            else
                $result = intval($varValue);
        }
        elseif ($arFieldType['type']=='varchar')
            $result = $DB->real_escape_string($varValue);
        else
            $result = $varValue;

        return $result;
    }

    static function add($varAddFields=false)
    {
        global $DB;

        $avColumns = self::getColumns();
        $avColumnsFull = self::getColumns('FULL');

        if ( is_array($varAddFields) && count($varAddFields)>0 )
        {
            foreach( $varAddFields as $key => $varAddFieldValue )
            {
                if ( in_array($key,$avColumns) )
                {
                    $varAddFieldValue = self::checkFieldValue($varAddFieldValue,$avColumnsFull[$key]->Type);
                    if($varAddFieldValue)
                    {
                        $arSqlFields[] = $key;
                        $arSqlValues[] = "'$varAddFieldValue'";
                    }
                }
                else
                    unset($varAddFields[$key]);
            }
        }
        $strSqlFields = ($arSqlFields ? implode(', ',$arSqlFields) : '');
        $strSqlValues = ($arSqlValues ? implode(', ',$arSqlValues) : '');
        $query = "INSERT INTO ".self::table." ({$strSqlFields}) VALUES ({$strSqlValues});";
        $sqlResult = $DB->query($query);

        return $sqlResult;
    }

    static function get ($arSelectedFields=false, $arFilter=false , $arLimit=false)
    {
        global $DB;

        $avColumns = self::getColumns();
        $avColumns[] = '*';
        $avColumnsFull = self::getColumns('FULL');

        if ( is_array($arSelectedFields) && count($arSelectedFields)>0 )
        {
            foreach( $arSelectedFields as $key=>$arSelectedField )
            {
                if ( !in_array($arSelectedField,$avColumns) )
                    unset($arSelectedFields[$key]);
            }

            $strSqlFields = ($arSelectedFields ? implode(', ',$arSelectedFields) : '');
        }

        if ( is_array($arFilter) && count($arFilter)>0 )
        {
            foreach( $arFilter as $key => $varFilterValue )
            {
                if ( in_array($key,$avColumns) )
                {
                    $varFilterValue = self::checkFieldValue($varFilterValue,$avColumnsFull[$key]->Type);
                    if($varFilterValue)
                        $arSqlFilters[] = $key."='$varFilterValue'";
                }
                else
                    unset($arFilter[$key]);
            }
            $strSqlFilters = ($arSqlFilters ? implode('AND ',$arSqlFilters) : '');
        }

        if ( !empty($strSqlFields) )
        {
            $query = "select {$strSqlFields} from ".self::table;
            if (!empty($strSqlFilters))
                $query .= " where $strSqlFilters";
            if ( is_array($arLimit) && sizeof($arLimit))
                $query .= " limit {$arLimit[0]},{$arLimit[1]}";
            elseif ( is_string($arLimit) && ctype_digit($arLimit) || is_int($arLimit) )
                $query .= " limit $arLimit";
            $query .= ";";

            $sqlResult = $DB->query($query);
            if ( $sqlResult->num_rows > 0 )
            {
                while ( $row = $sqlResult->fetch_object())
                    $arResult[] = $row;

                return $arResult;
            }
            else
                return [];
        }
        else
            throw new Exception('Отсутствуют корректные поля для выборки данных');
    }

    static function update($id,$varAddFields=false)
    {
        global $DB;

        $avColumns = self::getColumns();
        $avColumnsFull = self::getColumns('FULL');

        if ( is_array($varAddFields) && count($varAddFields)>0 )
        {
            foreach( $varAddFields as $key => $varAddFieldValue )
            {
                if ( in_array($key,$avColumns) )
                {
                    $varAddFieldValue = self::checkFieldValue($varAddFieldValue,$avColumnsFull[$key]->Type);
                    if($varAddFieldValue)
                        $arSqlFields[] = $key."='$varAddFieldValue'";
                }
                else
                    unset($varAddFields[$key]);
            }
        }
        $strSqlFields = ($arSqlFields ? implode(', ',$arSqlFields) : '');
        $query = "update ".self::table." set {$strSqlFields} where id={$id};";

        $sqlResult = $DB->query($query);

        return $sqlResult;
    }

    static function delete($id)
    {
        global $DB;

        if ( !ctype_digit($id)) return false;

        $query = "delete from ".self::table." where id={$id};";

        $sqlResult = $DB->query($query);

        return $sqlResult;
    }

    public function checkForValue ($arValues=false,$operator='AND')
    {
        global $DB;

        // проверяем входные переменные
        $avColumns = self::getColumns();
        $avColumnsFull = self::getColumns('FULL');
        if ( is_array($arValues) && count($arValues)>0 )
        {
            foreach( $arValues as $key => $arValue )
            {
                if( in_array($key,$avColumns) )
                {
                    $varValue = self::checkFieldValue($arValue,$avColumnsFull[$key]->Type);
                    if ( $varValue )
                        $arSqlStrValues[] = "$key = '$varValue'";
                }
                else
                    unset($arValues[$key]);
            }
        }
        else
            return true;
        if ( !in_array($operator,['AND','OR']) )
            $operator = 'AND';

        // формируем запрос
        if (count($arSqlStrValues)>1)
            $whereClose = implode(' '.$operator.' ',$arSqlStrValues);
        elseif (count($arSqlStrValues)==1)
            $whereClose = $arSqlStrValues[0];

        $query = "select id from ".self::table." where $whereClose";

        // выполняем запрос к базе данных
        $sqlResult = $DB->query($query);

        // возвращаем результат
        if ( $sqlResult->num_rows > 0 )
            return true;
        else
            return false;
    }
}