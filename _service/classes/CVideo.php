<?php

class CVideo
{
    use TDB;

    const table = 'playlist';

    static function rightUrl($wrongUrl)
    {
        $arUrl = explode('/',$wrongUrl);
        $code = str_replace('watch?v=','',$arUrl[3]);
        return "https://www.youtube.com/embed/$code";
    }
}