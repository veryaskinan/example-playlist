<?php

if ( $_GET['delete']=='y' && !empty($_GET['id']) && ctype_digit($_GET['id'])  )
{
    $deleteResult = CVideo::delete($_GET['id']);

    if ($deleteResult)
        header('Location: /');
    else
        die('Не удается удалить видеоролик.');
}
elseif ( !empty($_POST['edit']['submit']))
{
    if ( sizeof($DATA->error) == 0 )
    {
        if (!empty($_GET['id']))
        {
            if (ctype_digit($_GET['id']))
            {
                if ($DATA->video = CVideo::get(['id', 'title', 'url', 'created_at'], ['id' => $_GET['id']], 1)[0])
                {
                    if ($DATA->video->title != $_POST['edit']['title'])
                        $editFields['title'] = $_POST['edit']['title'];

                    if ($DATA->video->url != $_POST['edit']['url'])
                        $editFields['url'] = $_POST['edit']['url'];

                    if (is_array($editFields) && sizeof($editFields) > 0)
                        $updateResult = CVideo::update($_GET['id'], $editFields);

                    if ($updateResult)
                        header('Location: /');
                }
                else
                    die('Подмена id. Попытка взлома сайта. За вами уже выехали!');
            }
            else
                die('Подмена id. Попытка взлома сайта. За вами уже выехали!');
        }
        else
        {
            if (!empty($_POST['edit']['title']))
                $editFields['title'] = $DB->real_escape_string($_POST['edit']['title']);

            if (!empty($_POST['edit']['url']))
                $editFields['url'] = $DB->real_escape_string($_POST['edit']['url']);

            $editFields['created_at'] = time();

            if (is_array($editFields) && sizeof($editFields) > 0)
                $updateResult = CVideo::add($editFields);

            if ($updateResult)
                header('Location: /');
        }
    }
}
