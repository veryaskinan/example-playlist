<?php

$DATA->playlist = CVideo::get(['id','title','url','created_at']);

foreach ($DATA->playlist as $key => $video)
{
    $DATA->playlist[$key]->created_at = date('d.m.Y',$video->created_at);
}