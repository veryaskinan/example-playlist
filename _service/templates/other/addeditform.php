<? if (sizeof($DATA->error)>0) { ?>
    <div class='errors'>
        <? foreach($DATA->error as $error) { ?>
            <p class='error'><?=$error?></p>
        <? } ?>
    </div>
<? } ?>
<form action='' method='post'>
    <? if ( !empty($_GET['id']) ) { ?>
        <input type='hidden'name='edit[id]' value='<?=$_GET['id']?>'>
    <? } ?>
    <div class='title'>
        <label>Название видеоролика</label><br>
        <input type='text' name='edit[title]' value='<?=$DATA->video->title?>'>
    </div>
    <div class='url'>
        <label>Ссылка на видеоролик</label><br>
        <input type='text' name='edit[url]' value='<?=$DATA->video->url?>'>
    </div>
    <input class='button submit' type='submit' name='edit[submit]' value='Сохранить'>
    <a href='/' class='button cancel'>Отмена</a>
</form>