<h1><?=$DATA->video->title?></h1>

<div class='video'>
    <div class='play'>
        <?=$DATA->video->iframe?>
    </div>
    <div class='title'>
        <a href='/detail/?id=<?=$DATA->video->id?>'><?=$video->title?></a>
    </div>
    <div class='time'>
        Добавлено: <?=$DATA->video->created_at?>
    </div>
    <div class='link'>
        Ссылка на источник: <a href='<?=$DATA->video->url?>'><?=$DATA->video->url?></a>
    </div>
    <div class='controls'>
        <a href='/edit/?id=<?=$DATA->video->id?>' class='button controlls edit'>Редактировать</a>
        <a href='/edit/?delete=y&id=<?=$DATA->video->id?>' class='button controlls delete'>Удалить</a>
    </div>
</div>