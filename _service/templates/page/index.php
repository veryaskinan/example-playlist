<h1>Плейлист</h1>

<div class='playlist'>
    <? foreach ($DATA->playlist as $video) { ?>
        <div class='video'>
            <div class='title'>
                <a href='/detail/?id=<?=$video->id?>'><?=$video->title?></a>
            </div>
            <div class='time'>
                Добавлено: <?=$video->created_at?>
            </div>
            <div class='controls'>
                <a href='/edit/?id=<?=$video->id?>' class='button controlls edit'>Редактировать</a>
                <a
                    href='/edit/?delete=y&id=<?=$video->id?>'
                    class='button controlls delete'
                    onclick="return confirm ('Вы действительно хотите удалить видеоролик\n<?=$video->title?>?');"
                >
                    Удалить
                </a>
            </div>
        </div>
    <? } ?>
    <div class='add'>
        <a href='/add' class='button add'>Добавить</a>
    </div>
</div>