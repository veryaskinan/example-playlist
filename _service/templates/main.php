<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="/_service/css/main.css">
        <title><?= $PAGE->title ?></title>
    </head>
    <body>
        <div class='mainBlock'>
            <? CPage::useTemplate($PAGE->template); ?>
        </div>
    </body>
</html>