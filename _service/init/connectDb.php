<?php

$connectionSettings = [
    'host' => 'localhost',
    'userName' => 'test',
    'password' => 'test1234',
    'dbName' => 'test'
];

$DB = new mysqli(
    $connectionSettings['host'],
    $connectionSettings['userName'],
    $connectionSettings['password'],
    $connectionSettings['dbName']
);