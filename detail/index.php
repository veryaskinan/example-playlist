<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/_service/init/init.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/_service/controllers/page_detail.php');

$PAGE->template = 'detail';
$PAGE->title = $DATA->video->title;

require_once($_SERVER['DOCUMENT_ROOT'].'/_service/templates/main.php');